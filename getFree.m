function F = getFree (X)
% getFree  Returns a mask corresponding to empty cells.
%
%   F = getFree (X);
%
% Given a matrix X(1:m, 1:n), returns a "mask" matrix, F(1:m, 1:n),
% such that F(i,j) = 1 if X(i, j) is 0 _and_ is not on the boundary;
% otherwise, F(i,j) = 0.
  
  [m, n] = size (X);
  I = (2:(m-1));
  J = (2:(n-1));

  F = zeros (m, n);
  F(I, J) = ((X(I, J) == 0) & ~((X(I-1, J-1) ~= 0) & (X(I-1, J) ~= 0) & (X(I-1, J+1) ~= 0) & (X(I, J-1) ~= 0)       & (X(I, J+1) ~= 0)& (X(I+1, J-1) ~= 0) & (X(I+1, J) ~= 0) & (X(I+1, J+1) ~= 0)));
end

% eof
