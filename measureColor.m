function S = measureColor (X)
  [m, n] = size (X);
  I = (2:(m-1));
  J = (2:(n-1));
  
  % Measure the "color" of the neighborhood at each house
  C = zeros (size (X));
  C(I, J) = C(I, J) + X(I-1, J-1) + X(I-1, J) + X(I-1, J+1);
  C(I, J) = C(I, J) + X(I,   J-1) + X(I,   J) + X(I, J+1);
  C(I, J) = C(I, J) + X(I+1, J-1) + X(I+1, J) + X(I+1, J+1);

  % Return the dominant color
  S = sign (C);
end

% eof
