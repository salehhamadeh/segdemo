function M = measureSegregation( X )
%measureSegregation Returns a number between 0.0 and 1.0 that indicates
%   the population's degree of segregation
%
%   The function calculates the degree of segregation by examining each
%   cell and its neighbors. It averages the proportions of the neighbors
%   who are of the same race.

  [m, n] = size (X);
  I = (2:(m-1));
  J = (2:(n-1));
  
  % Occupied cells
  O = find(X ~= 0);
  
  % Measures how many neighbors of the same race surround each cell
  % (includes empty cells for simplicity's purpose. Cells are cleared later
  % in the code)
  S = zeros(size(X));
  S(I, J) = S(I, J) + (X(I-1, J-1) == X(I, J)) + (X(I-1, J) == X(I, J)) + (X(I-1, J+1) == X(I, J));
  S(I, J) = S(I, J) + (X(I, J-1) == X(I, J))+ (X(I, J+1) == X(I, J));
  S(I, J) = S(I, J) + (X(I+1, J-1) == X(I, J)) + (X(I+1, J) == X(I, J)) + (X(I+1, J+1) == X(I, J));
  
  % Measures how many neighbors (non-empty cells) surround each cell
  N = zeros (size (X));
  N(I, J) = N(I, J) + abs(X(I-1, J-1)) + abs(X(I-1, J)) + abs(X(I-1, J+1));
  N(I, J) = N(I, J) + abs(X(I,   J-1)) +                + abs(X(I, J+1));
  N(I, J) = N(I, J) + abs(X(I+1, J-1)) + abs(X(I+1, J)) + abs(X(I+1, J+1));
  
  % Find the proportions of neighbors of the same race
  M = S ./ N;
  
  % Set empty and boundary cells equal to zero
  M(X == 0) = 0;
  M(isnan(M)) = 0;
  
  % Return the average of all proportions
  sum(sum(M));
  size(O);
  M = sum(sum(M)) ./ size(O);
end

