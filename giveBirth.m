function [Y, nBorn] = giveBirth( X )
%giveBirth Returns a grid of the population after giving birth
%
%   Empty cells that are surrounded by 6 or more cells of the same race
%   will become equal to those cells.

  [m, n] = size (X);
  I = (2:(m-1));
  J = (2:(n-1));
  
  % Empty cells mask
  E = (X == 0);
  
  % Measures how many neighbors of the race -1 surround each empty cell
  % (includes non-empty cells for simplicity's purpose. Cells are cleared
  % late in the code)
  A = zeros(size(X));
  A(I, J) = A(I, J) + (X(I-1, J-1) == -1) + (X(I-1, J) == -1) + (X(I-1, J+1) == -1);
  A(I, J) = A(I, J) + (X(I, J-1) == -1)+ (X(I, J+1) == -1);
  A(I, J) = A(I, J) + (X(I+1, J-1) == -1) + (X(I+1, J) == -1) + (X(I+1, J+1) == -1);
  
  % Clear non-empty cells from A
  A = A .* E;
  
  % Measures how many neighbors of the race +1 surround each empty cell
  % (includes non-empty cells for simplicity's purpose. Cells are cleared
  % late in the code)
  B = zeros(size(X));
  B(I, J) = B(I, J) + (X(I-1, J-1) == 1) + (X(I-1, J) == 1) + (X(I-1, J+1) == 1);
  B(I, J) = B(I, J) + (X(I, J-1) == 1)+ (X(I, J+1) == 1);
  B(I, J) = B(I, J) + (X(I+1, J-1) == 1) + (X(I+1, J) == 1) + (X(I+1, J+1) == 1);
  
  % Clear non-empty cells from B
  B = B .* E;
  
  % Initialize the return value to X
  Y = X;
  
  % Give birth
  Y(A >= 6) = -1;
  Y(B >= 6) = 1;
  
  % Store the number of cells born in nBorn
  nBorn = length(find(A >= 6)) + length(find(B >= 6));
end

