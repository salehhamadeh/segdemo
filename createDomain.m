function D = createDomain (m, n, p, r)
 % createDomain  Returns a random two-class population grid.
 %
 %   D = createDomain (m, n, p, r)
 %
 % Returns an (m+2) x (n+2) rectilinear grid of cells, D, initialized
 % randomly as follows.
 %
 % Each cell D(i, j) has one of three possible values, {-1, 0, 1}. The
 % boundary cells -- D(1,:), D(m+2,:), D(:,1), and D(:,n+2) -- are set
 % to 0. Each interior cell has the value 0 with probability p and has
 % a value of +1 or -1 with probability (1-p). The ratio of "+1" cells
 % to "-1" cells will be approximately r.
  
  q = 1 - p; % "occupied" ("+1" or "-1") rate

  % Populate the domain, initially with all "+1" values
  X = zeros (m+2, n+2); 
  I = 2:(m+1); % row indices of "inner" zone
  J = 2:(n+1); % column indices of "inner" zone
  X(I, J) = (rand (m, n) <= q);

  % Get a list of the non-empty cell positions
  [I_set, J_set] = find (X);
  n_set = length (I_set); % # of non-empty cells

  % Choose a subset of positions to "flip" from "+1" to "-1"
  n_flip = floor (n_set / (r + 1)); % # of values to convert to "-1"
  K_flip = randperm (n_set);
  I_flip = I_set(K_flip(1:n_flip));
  J_flip = J_set(K_flip(1:n_flip));

  % Store "-2" at the positions to flip
  X_flip = sparse (I_flip, J_flip, -2, m+2, n+2);

  % Flip the subset
  D = X + X_flip;
end

% eof
